import React from 'react';
import { Actions, Scene } from 'react-native-router-flux';
import Page01 from './page01_scene';
import Page02 from './page02_scene';

const scenes = Actions.create(
    <Scene key="root">
        <Scene key="page01" component={Page01} />
        <Scene key="page02" component={Page02} />        
    </Scene>
)
export default scenes;