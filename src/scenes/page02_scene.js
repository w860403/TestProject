import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import Page02 from '../containers/page02';

const Page02Scene = () => {
    return (
        <View style={styles.container}>
            <Page02 />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
export default Page02Scene;
