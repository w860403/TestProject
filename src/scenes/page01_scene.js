import React from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import Page01 from '../containers/page01';

const Page01Scene = () => {
    return (
        <View style={styles.container}>
            <Page01 />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
})
export default Page01Scene;
