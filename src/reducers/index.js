import { combineReducers } from 'redux';
import saveName_reducer from './saveName_reducer';

const root_reducers = combineReducers({
    name: saveName_reducer
});

export default root_reducers;