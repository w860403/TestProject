import { connect } from 'react-redux';
import Page01Component from '../components/page01';
import { name_change, member_data } from '../actions/page01_action';
import { Actions } from 'react-native-router-flux';

const mapStateToProps = (state) => ({
    // login_status: state.login_status,
    // login_data: state.login_data
    name:state.name,
});

const mapDispatchToProps = (dispatch) => ({
    onNextPageButtonClick: () => {
        Actions.page02();
    },
    onNameChange: (name) => {
        dispatch(name_change(name));
    },
    onMemberButtonClick: () => {
        dispatch(member_data());
    }
}
);

class Page01Container extends Page01Component {
    constructor(props) {
        super(props);
    }
    componentWillReceiveProps(nextProps) {
        const { name: previous_name } = this.props;
        const { name } = nextProps;
        if (previous_name != name) {
            this.setState({
                nameState:name
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Page01Container);
