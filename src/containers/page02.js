import { connect } from 'react-redux';
import Page02Component from '../components/page02';
import { Actions } from 'react-native-router-flux';

const mapStateToProps = (state) => ({
    name: state.name
});

const mapDispatchToProps = (dispatch) => ({
    onPreviousPageButtonClick: () => {
        Actions.pop();
    }
}
);

class Page02Container extends Page02Component {
    constructor(props) {
        super(props);
    }
    componentWillReceiveProps(nextProps) {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Page02Container);
