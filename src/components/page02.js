import React, { Component } from 'react';
import {
    Text,
    TextInput,
    View,
    Button
} from 'react-native';

class TestProject extends Component {
    render() {
        return (
            <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                <Text style={{fontSize:20}}>Hello, {this.props.name}</Text>
                <Button
                    onPress={() => this.props.onPreviousPageButtonClick()}
                    title="上一頁" />
            </View>
        );
    }
}
export default TestProject;