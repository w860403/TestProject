import React, { Component } from 'react';
import {
    Text,
    TextInput,
    View,
    Button
} from 'react-native';

class TestProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            num01: 0,
            num02: 0
        }
    }
    render() {
        return (
            <View style={{ alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                <Text style={{ fontSize: 20 }}>{this.state.nameState}</Text>
                <Text style={{ fontSize: 20 }}>請輸入姓名</Text>
                <TextInput
                    style={{ borderWidth: 1, height: 30, width: 100 }}
                    onChangeText={
                        (name) => this.props.onNameChange(name)
                    }
                />
                <Button
                    onPress={() => this.props.onNextPageButtonClick()}
                    title="確定" />
                <Button
                    onPress={() => this.props.onMemberButtonClick()}
                    title="取得" />
            </View>
        );
    }
}
export default TestProject;